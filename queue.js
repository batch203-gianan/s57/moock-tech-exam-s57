let collection = [];

// Write the queue functions below.
//  1.  Print queue elements
const print = () => {
    return collection;
}

//  2.  Enqueue a new element
const enqueue = (item) => {
    collection[collection.length] = item;
    return collection;
}

//  3.  Dequeue the first element
const dequeue = () => {
    for(let i=0; i<collection.length; i++){
        collection[i] = collection[i+1]
    }
    collection.length--;
    return collection;
}

//  4.  Get the first element
const front = () => {
    return collection[0];
}

//  5.  Get the queue size
const size = () => {
    return collection.length
}

//  6.  return false if qeueue is not empty
const isEmpty = () => {
    return collection.length === 0;
}
// Export create queue functions below.
module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};